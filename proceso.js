let salir_busq = document.querySelector("#salir_busqueda");
salir_busq.disable = true;
let titulo = document.querySelector('#titulo');
let seccion_producto = document.querySelector('#producto');
let descripcion = document.querySelector('#desc');
let url_imagen = document.querySelector('#dirImg');
let boton_uno = document.querySelector('#btn_uno');
let boton_dos = document.querySelector('#btn_dos');

(function () {
    if (!window.openDatabase) {
        alert('Este navegador no soporta el API WebSql');
    }
})();

let dbComercio;

function openDB() {
    console.log("Abriendo base de datos");
    dbComercio = openDatabase(
        "dbComercio",
        "1.0",
        "Esta es una base de datos del lado del cliente",
        3 * 1024 * 1024
    );
}

function creaTabla() {
    console.log("Creando tabla");
    dbComercio.transaction(function (tx) {
        tx.executeSql(
            "CREATE TABLE IF NOT EXISTS Articulos ( id integer primary ker autoincrement, descripcion, imagen)"
        );
    });
}

let producto = {
    codigo: 0,
    descripcion: "",
    imagen: "",

};

let des, img;

function agregar() {
    console.log("Agregando registro");
    creaTabla();
    dbComercio.transaction(function (tx) {

        des = descripcion.value.trim();
        img = url_imagen.value.trim();
        if (des.length === 0 || img.length === 0) return;

        producto.descripcion = des;
        producto.imagen = img;

        tx.executeSql(
            "   INSERT INTO Articulos (descripcion, imagen) VALUES (?,?)",
            [producto.descripcion, producto.imagen],
            itemInserted
        );
    });
    muestraTabla();

}
function itemInserted(tx, results) {
    console.log("id:", results.insertID);
}

let cantidad;
//AHORA MUESTRA () LEE TODOS LOS ARTICULOS DE LA TABLA Y LLAMA A ARMATEMPLATE
function muestraTabla() {
    console.log("Mostrando tabla ");
    dbComercio.transaction(function (tx) {
        tx.executeSql("SELECT * FROM Articulos", [], function (tx, results) {
            cantidad = results.rows.length;
            armaTemplate(results);
        });
    });
}

function armaTemplate(results) {
    let template = '', row;

    for (let i = 0; i < cantidad; i++) {
        row = results.rows.item(i);

        producto.id = row.id;
        producto.descripcion = row.descripcion;
        producto.imagen = row.imagen;

        template += `<article>
        <div class ="trash"  onclick="eliminarItem(${producto.id})"><img src="eliminar.png"></div>
        <div class ="edit"  onclick="editarItem(${producto.id})"><img src="edit1.png"></div>
        <h3 class ="descripcion">${producto.descripcion}</h3>
        <img src="${producto.imagen}" class ="imagen">
    </article>`
    }
    document.querySelector("#producto").innerHTML = template;
}

//LISTADO LEE LA TABLA, PASA LOS ARTICULOS A LOCALSTORAGE, Y LLAMA A RESUTLADOS.HTML
function listado() {
    let arrayProductos = [], row;
    dbComercio.transaction(function (tx) {
        tx.executeSql("SELECT * FROM  Articulos", [], function (tx, results) {
            cantidad = results.rows.length;
            for (let i = 0; i < cantidad; i++) {
                row = results.rows.item(i);
                console.log(row);
                arrayProductos[i] = row;
            }
            if (cantidad > 0) {
                localStorage.setItem('articulos', JSON.stringify(arrayProductos));
                location.href = 'resultados.html';
            }
        });
    });
}

function eliminarItem(nroProd) {
    console.log("Borrando registro: ", nroProd);

    dbComercio.transaction(function (tx) {
        tx.executeSql('DELETE FROM Articulos WHERE id=?', [nroProd],
            function (tx, result) {
                console.log(result);
                console.log('Registro borrado satisfactoriamente!');
            },
            function (tx, error) {
                console.log(error);
            });
    }, null);
    muestraTabla();
};

function editarItem(nroProd) {
    console.log("Actualizando registro: ", nroProd);
    muestraTablaSinBtns();
    document.querySelector("#titulo").innerHTML = "Edición de Producto";
    boton_uno.value = "Modificar";
    boton_uno.classList.add("color_green");
    boton_dos.value = "Cancelar";
    boton_dos.classList.add("color_red");
    boton_uno.setAttribute("onclick", `modificar(${nroProd})`);
    boton_dos.setAttribute("onclick", "limpiar_cancelar()");

    dbComercio.transaction(function (tx) {
        tx.executeSql("SELECT * FROM Articulos WHERE id=?", [nroProd],
            function (tx, results) {
                cantidad = results.rows.length;
                if (cantidad !== 1) console.log("Error, se encontraron más de un registro iguales en la tabla");

                producto = results.rows.item(0);

                descripcion.value = producto.descripcion;
                url_imagen.value = producto.imagen;
            });
    });
}

function muestraTablaSinBtns() {
    console.log("Mostrando tabla sin botones");
    dbComercio.transaction(function (tx) {
        tx.executeSql("SELECT * FROM Articulos", [], function (tx, results) {
            armaTemplateSinBtns(results);
        });
    });
}

function armaTemplateSinBtns(results) {
    let template = '', row;

    for (let i = 0; i < cantidad; i++) {
        row = results.rows.item(i);

        producto.id = row.id;
        producto.descripicion = row.descripcion;
        producto.imagen = row.imagen;

        template += `<article>
            <h3 class="descripcion">${producto.descripcion}</h3>
            <img src="${producto.imagen}" class="imagen">
            </article>`
    }
    document.querySelector("#producto").innerHTML = template;

}

function limpiar_cancelar() {
    descripcion.value = "";
    url_imagen.value = "";
    titulo.innerHTML = "Nuevo Producto";
    boton_uno.value = "Agregar";
    boton_uno.classList.remove("color_green");
    boton_dos.value = "Listado";
    boton_dos.classList.remove("color_red");
    boton_uno.setAttribute("onclick", "agregar()");
    boton_dos.setAttribute("onclick", "listado()");
    muestraTabla();
}

function modificar(nroProd) {
    console.log("Modificando regristro:", nroProd);
    des = descripcion.value.trim();
    img = url_imagen.value.trim();
    if (des.length === 0 || img.length === 0) return;

    producto = {
        id: nroProd,
        descripcion: des,
        imagen: img
    }

    dbComercio.transaction(function (tx) {
        console.log(producto.descripcion, " - ", producto.imagen);
        tx.executeSql(
            "UPDATE Articulos SET descripcion=?, imagen=? WHERE id=?",
            [producto.descripcion, producto.imagen, producto.id]
        );
    });
    limpiar_cancelar();
}

function busqueda() {
    document.querySelector("#agrego").classList.add("disable");
    salir_busq.disable = false;
    let aBuscar = document.querySelector("#aBuscar").value;
    if (aBuscar.trim().length === 0) {
        muestraTablaSinBtns();
    } else {
        console.log("Leyendo con select lo pedido");
        dbComercio.transaction(function (tx) {
            let q = `SELECT * FROM Articulos WHERE descripcion LIKE ?`;
            console.log(q);
            tx.executeSql(q, [aBuscar.trim() + "%"], function (tx, results) {
                cantidad = results.rows.length;
                console.log(cantidad);
                armaTemplateSinBtns(results);
            });
        });
    }

}



function salir_busqueda() {
    aBuscar.value = "";
    muestraTabla();
    document.querySelector("#agrego").classList.remove("disable");
    salir_busq.disable = true;
}

function dropTable() {
    dbComercio.transaction(function (tx) {
        tx.executeSql(
            "DROP TABLE Articulos",
            [],
            function (tx, results) {
                console.log("Table Dropped");
            },
            function (tx, error) {
                console.log("Error capturado: " + error.message);
            }
        );
    });
    document.querySelector("#producto").innerHTML = "";
}

function main() {
    openDB();
    creaTabla();
    muestraTabla();
}

main();
